//@todo:impletment save by user and limit who can edit
//@todo:impletment WYSIWYG editor
Template.editPost.events({
    "submit .editSave":function(e){
        //prevent form from sending
        e.preventDefault();
        //get data from the form
        var title = e.target.title.value;
        var content = e.target.content.value;
        var isActive = e.target.isActive.checked;

        //save the new entry's
        BlogList.update(this._id,{
            $set:{
                title:title,
                content:content,
                date:new Date,
                isActive:isActive
            }
        })
        //redirect to the home page
        Router.go("/");
    }
});

//enable debug for the file upload

Template.editPost.events({
    'change .fileUpload': function(event, template) {
        var postId = this._id;
        var files = event.target.files;
        for (var i = 0, ln = files.length; i < ln; i++) {
            Images.insert(files[i], function (err, fileObj) {
                //save the image id to the post
                BlogList.update(postId,{$push:{images:fileObj._id}});
            });
        };
    },
    "click .remove-img":function(e,template){
       BlogList.update(template.data._id,{$pull:{images:this._id}})
    },
    "click .edit-img":function(e,template){
        $link = $(e.target.parentElement.getElementsByTagName("a")[0]);
        $img = $(e.target.parentElement.getElementsByTagName("img")[0]);
        $save = $(e.target.parentElement.getElementsByClassName("save-crop")[0]);
        imageId = template.data._id;
        imageInDb = Images.find(imageId);

        $link.on("click",function(e){
            e.preventDefault()
        });
        var imgData={};
        $img.cropper({
            aspectRatio: 16 / 9,
            autoCropArea: 0.65,
            strict: false,
            guides: false,
            highlight: false,
            dragCrop: false,
            cropBoxMovable: false,
            cropBoxResizable: false,
            crop: function(e) {
                // Output crop data
                imgData.x = e.x;
                imgData.y = e.y;
                imgData.width = e.width;
                imgData.height = e.height;
            }
        });
        $save.toggle();
        $save.on("click",function(e){
            $img.cropper('destroy');
            $save.toggle();

            Meteor.call('editImageOnServer',imageId,imgData,function(err,response){
                console.log("test");
            })
        });
        //BlogList.update(template.data._id,{$pull:{images:this._id}})
    }
});
Template.editPost.helpers({
    imagesSource:function(){
        if(this.images){
            return Images.find({_id:{$in:this.images}});
        }
    }
});