Template.home.helpers({
    createdAtFormatted: function (par) {
        return moment(par).format('DD-MM-YYYY');
    },
    shortContent:function(content,numberOfWords){
        if(content){
            return content.split(' ').slice(0,numberOfWords).join(' ')
        }
    },
    getFirstImage:function(ref){
        if(ref){
            //console.log(Images.findOne({_id:ref[0]}));
            //the first image is the favorite
            return Images.findOne({_id:ref[0]});
        }
    }
});
Template.home.events({
    "click .deletePost":function(){
        BlogList.remove(this._id)
    }
});

