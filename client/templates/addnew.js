//@todo:impletment save by user and limit who can edit
//@todo:impletment WYSIWYG editor
Template.addnew.events({
    "submit .editSave":function(e){
        //prevent form from sending
        e.preventDefault();
        //get data from the form
        var title = e.target.title.value;
        var content = e.target.content.value;
        var isActive = e.target.isActive.checked;
        var files = e.target.fileUpload.files;

        //save the new entry's
        var postId = BlogList.insert(
         {
            title:title,
            content:content,
            date:new Date,
            isActive:isActive
        });

        for (var i = 0, ln = files.length; i < ln; i++) {
            Images.insert(files[i], function (err, fileObj) {
                //save the image id to the post
                BlogList.update(postId,{$push:{images:fileObj._id}});
            });
        };


        //redirect to the home page
        Router.go("/");
    }
});

//enable debug for the file upload

Template.addnew.events({
    'change .fileUpload': function(event, template) {
        var postId = this._id;
        var files = event.target.files;
        for (var i = 0, ln = files.length; i < ln; i++) {
            Images.insert(files[i], function (err, fileObj) {
                //save the image id to the post
                BlogList.update(postId,{$push:{images:fileObj._id}});
            });
        };
    },
    "click .remove-img":function(e,template){
        BlogList.update(template.data._id,{$pull:{images:this._id}})
    }
});
Template.addnew.helpers({
    imagesSource:function(value){
        if(this.images){
            return Images.find({_id:{$in:this.images}});
        }
    }
});