BlogList = new Mongo.Collection("posts");

Images = new FS.Collection("images", {
  stores: [
    new FS.Store.FileSystem("images", {path: "../../../../../fileUploads"}),
    new FS.Store.FileSystem("thumbs",{
      path: "../../../../../fileUploads/thumbs",
      transformWrite: function(fileObj, readStream, writeStream) {;
        gm(readStream, fileObj.name()).resize(800,800).stream().pipe(writeStream);
      }
    })]
});

Router.configure({
  layoutTemplate: 'appBody',
  loadingTemplate: 'loadingtmp',
  yieldTemplates: {
    'header': {to: 'header'},
    'footer': {to: 'footer'}
  }
});
Router.map(function(){
  this.route('home', {path: '/',
    loadingTemplate: 'loadingtmp',
    data: function() {
      templateData = { posts: BlogList.find({}) };
      return templateData;
    }
  });

  this.route('addnew', {path: 'addnew',
    loadingTemplate: 'addnew'
  });

  this.route('editPost', {path: '/edit/:postid',
    loadingTemplate: 'loadingtmp',
    data: function() {
      return BlogList.findOne({_id: this.params.postid});
    }
  });
});