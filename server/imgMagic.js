Meteor.methods({
    editImageOnServer: function (imageId,imgData) {
        var imageIn = Images.find(imageId);
        var readStream = imageIn.createReadStream('thu');
        var writeStream = imageIn.createWriteStream('images');
        gm(readStream).crop(imgData.x,imgData.y,imgData.width,imgData.height).stream().pipe(writeStream);
    }
});